<a href="<?= $slidePageLink ?>" class="slide-button" id="<?= 'slide_button_' . $id ?>">
    <?= $slideSubmitButtonText ?>
</a>
<style type="text/css">
    #slide_button_<?= $id ?> {
        <?= getButtonGradient($slideBorderColor, 25, CalculateRgb::TYPE_DARKEN) ?>
    }
    #slide_button_<?= $id ?>:hover {
        <?= getButtonGradient($slideBorderColor, 50, CalculateRgb::TYPE_DARKEN) ?>
    }
</style>