<?php

/**
 * Template file.
 *
 * @package    WordPress
 * @subpackage My_Theme_Development
 * @since      My Theme Development 1.0
 **/

get_header();
?>

    <div id="home-content">
        <?= get_template_part('slider/slider', 'slider'); ?>
        <?php
        $options = get_option('healthnetwm_theme_options');
        if ($options && !empty($options)):
            ?>
            <?= get_template_part('facts/facts', 'facts'); ?>
            <section id="mission">
                <p><?= $options['mission'] ?></p>
            </section>
            <section id="callsToAction">
                <div class="left">
                    <a href="<?= $options['bottom_left_url'] ?>">
                        <div class="img-wrapper"><img src="<?= $options['bottom_left_image'] ?>"/></div>
                        <h3><?= $options['bottom_left_title'] ?></h3>

                        <p><?= $options['bottom_left_content_area'] ?></p>
                    </a>
                </div>
                <div class="middle">
                    <a href="<?= $options['bottom_middle_url'] ?>">
                        <div class="img-wrapper"><img src="<?= $options['bottom_middle_image'] ?>"/></div>
                        <h3><?= $options['bottom_middle_title'] ?></h3>

                        <p><?= $options['bottom_middle_content_area'] ?></p>
                    </a>
                </div>
                <div class="right">
                    <a href="<?= $options['bottom_right_url'] ?>">
                        <div class="img-wrapper"><img src="<?= $options['bottom_right_image'] ?>"/></div>
                        <h3><?= $options['bottom_right_title'] ?></h3>

                        <p><?= $options['bottom_right_content_area'] ?></p>
                    </a>
                </div>
            </section>
        <?php endif ?>
        <section id="home-events">
            <?php if (is_active_sidebar('home_widgets')) : ?>
                <div id="primary-widgets" class="widget-area" role="complementary">
                    <?php dynamic_sidebar('home_widgets') ?>
                </div>
            <?php endif ?>
        </section>
        <?= get_template_part('quotes/quotes', 'quotes'); ?>
    </div>
<?php get_footer() ?>
