<?php

$quotes = new WP_Query(['post_type' => 'quote', 'orderby' => 'rand', 'posts_per_page' => 1]);

/**
 * Setup the default fields
 */
$quoteAuthor = '';
?>
<?php if ($quotes->have_posts()) : ?>
    <section id="quotes">
        <?php while($quotes->have_posts()) : ?>
            <?php $quotes->the_post() ?>
            <?php extract(getFactFields($quotes), EXTR_OVERWRITE) ?>
            <div class="blockquote">
                <div class="quote"><span class="quotemark">"</span><?= str_replace('</p>', '', str_replace('<p>', '', get_the_content())) ?><span class="quotemark">"</span></div>
                <p class="author"><?= $quoteAuthor ?></p>
            </div>
            <?php wp_reset_postdata() ?>
        <?php endwhile ?>
    </section>
<?php endif ?>
