<?php

$slides = new WP_Query(['post_type' => 'slides', 'posts_per_page' => 10]);

/**
 * Setup the default fields
 */
$slideImage = '';
$slideAlternateText = '';
$slideSecondaryImage = '';
$slideSecondaryAlternateText = '';
$slideSecondaryBackgroundColor = '';
$slidePageLink = '';
$slideSubmitButtonText = '';
$slideBorderColor = '';
$slideButtonTextColor = '';

$slideColors = [];

?>
<?php if ($slides->have_posts()) : ?>
<section id="slider" class="hidden">
    <div class="flexslider" style="border: 4px solid #fff; border-radius: 0;">
        <div class="slides">
            <?php while ($slides->have_posts()) : ?>
                <?php $slides->the_post() ?>
                <?php extract(getSliderFields($slides), EXTR_OVERWRITE) ?>
                <?php $slideColors[] = $slideBorderColor; ?>
                <div class="slide">
                    <img class="slide-image" src="<?= getRelativeUrl($slideImage) ?>" alt="<?= $slideAlternateText ?>" />
                    <div class="slide-body">
                        <h1 class="slide-title">
                            <?php the_title() ?>
                        </h1>
                            <div class="slide-content">
                            <?php the_content() ?>
                        </div>
                        <?php if (!empty($slideSubmitButtonText) && !empty($slidePageLink)): ?>
                            <?php include locate_template('slider/slider-button.php') ?>
                        <?php endif ?>
                    </div>
                    <?php if (!empty($slideSecondaryImage)): ?>
                        <?php include locate_template('slider/slider-secondary.php') ?>
                    <?php endif ?>
                </div>
                <?php wp_reset_postdata() ?>
            <?php endwhile ?>
        </div>
    </div>
    <script type="text/javascript">
        var borderColor = '<?= $slideBorderColor ?>';
        var sliderColors = <?= json_encode(array_reverse($slideColors)) ?>;
        var currSlide = 1; //because we're starting on the first transition
        var maxSlides = <?= $slides->post_count ?>;

        jQuery(document).ready(function() {
            jQuery('.flexslider').css('border', '4px solid ' + borderColor);
        });

        jQuery(window).load(function() {
            jQuery('#slider').removeClass('hidden');

            jQuery('.flexslider').flexslider({
                animation: "slide",
                selector: ".slides > div",
                before: changeSliderColor
            });
        });

        function changeSliderColor()
        {
            jQuery('.flexslider').css('border', '4px solid ' + sliderColors[currSlide]);
            currSlide++;
            if(currSlide >= maxSlides) currSlide = 0;
        }
    </script>
</section>
<?php endif ?>
