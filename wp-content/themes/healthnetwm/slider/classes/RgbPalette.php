<?php

/**
 * Class RgbPalette
 */
class RgbPalette
{
    /**
     * @var integer
     */
    protected $red;

    /**
     * @var integer
     */
    protected $green;

    /**
     * @var integer
     */
    protected $blue;

    /**
     * @return integer
     */
    public function getRed()
    {
        return $this->red;
    }

    /**
     * @param integer $red
     * @return $this
     */
    public function setRed($red)
    {
        $this->red = $red;
        return $this;
    }

    /**
     * @return integer
     */
    public function getGreen()
    {
        return $this->green;
    }

    /**
     * @param integer $green
     * @return $this
     */
    public function setGreen($green)
    {
        $this->green = $green;
        return $this;
    }

    /**
     * @return integer
     */
    public function getBlue()
    {
        return $this->blue;
    }

    /**
     * @param integer $blue
     * @return $this
     */
    public function setBlue($blue)
    {
        $this->blue = $blue;
        return $this;
    }
}
