<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <title><?php wp_title('|', true, 'right'); ?><?php bloginfo('name'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="<?php bloginfo('charset'); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link rel="shortcut icon" href="<?php echo esc_url(get_stylesheet_directory_uri()); ?>/favicon.ico"/>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page-wrapper"> <?php // closed in footer.php ?>

    <div id="header-wrapper">
        <header id="top-header">
            <div class="logo-container">
                <a href="<?php echo esc_url(home_url('/')); ?>">
                    <img src="<?php echo esc_url(get_template_directory_uri() . '/assets/images/hn-logo-white.png'); ?>" alt="Health Net of West Michigan logo" class="logo"/>
                </a>
            </div>
            <div class="hamburger-menu open"><img src="/wp-content/themes/healthnetwm/assets/images/hamburger.svg" alt="Open the primary menu"></div>
            <nav id="main-nav">
                <?php wp_nav_menu(['theme_location' => 'main_nav']); ?>
            </nav>
            <nav id="top-nav">
                <?php wp_nav_menu(['theme_location' => 'top_nav']); ?>
            </nav>
            <div id="top-search">
                <?php get_search_form(); ?>
            </div>
        </header>
    </div>
    <div id="top-sub-header"><p>of West Michigan</p></div>

    <div id="main-content-container"><?php // closed in footer.php ?>



