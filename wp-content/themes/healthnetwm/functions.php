<?php

function healthnet_add_styles()
{
    // Register the branding CSS
    wp_enqueue_style(
        'branding',
        get_template_directory_uri() . '/assets/css/branding.css',
        [],
        '20141107',
        'all'
    );

    // Register the flexslider CSS
    wp_enqueue_style(
        'flexslider',
        get_template_directory_uri() . '/assets/scripts/flexslider/flexslider.css',
        [],
        '20141108',
        'all'
    );

    wp_enqueue_style(
        'fontawesome',
        '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css',
        [],
        '20141108',
        'all'
    );
}

add_action('wp_enqueue_scripts', 'healthnet_add_styles');

function healthnet_add_scripts()
{
    wp_enqueue_script(
        'healthnetjs',
        get_template_directory_uri() . '/assets/js/healthnet.js',
        ['jquery'],
        '20141108',
        true
    );
    wp_enqueue_script(
        'jquery.flexslider',
        get_template_directory_uri() . '/assets/scripts/flexslider/jquery.flexslider-min.js',
        ['jquery'],
        '20141108',
        true
    );
}

add_action('wp_enqueue_scripts', 'healthnet_add_scripts');

/*
* Creating a function to create our CPT
*/

function custom_post_type()
{
    $slides = [
        'label' => __('slides', 'healthnetwm'),
        'description' => __('Slides for homepage', 'healthnetwm'),
        'labels' => [
            'name' => _x('Slides', 'Post Type General Name', 'healthnetwm'),
            'singular_name' => _x('Slide', 'Post Type Singular Name', 'healthnetwm'),
            'menu_name' => __('Slides', 'healthnetwm'),
            'parent_item_colon' => __('Parent Slide', 'healthnetwm'),
            'all_items' => __('All Slides', 'healthnetwm'),
            'view_item' => __('View Slide', 'healthnetwm'),
            'add_new_item' => __('Add New Slide', 'healthnetwm'),
            'add_new' => __('Add New', 'healthnetwm'),
            'edit_item' => __('Edit Slide', 'healthnetwm'),
            'update_item' => __('Update Slide', 'healthnetwm'),
            'search_items' => __('Search Slide', 'healthnetwm'),
            'not_found' => __('Not Found', 'healthnetwm'),
            'not_found_in_trash' => __('Not found in Trash', 'healthnetwm'),
        ],
        'supports' => [
            'title',
            'editor',
            'revisions',
            'custom-fields',
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => false,
        'show_in_admin_bar' => true,
        'menu_position' => 30,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    ];
    register_post_type('slides', $slides);

    $staff = [
        'label' => __('staff', 'healthnetwm'),
        'description' => __('Staff for homepage', 'healthnetwm'),
        'labels' => [
            'name' => _x('Staff', 'Post Type General Name', 'healthnetwm'),
            'singular_name' => _x('Staff', 'Post Type Singular Name', 'healthnetwm'),
            'menu_name' => __('Staff', 'healthnetwm'),
            'parent_item_colon' => __('Parent Staff', 'healthnetwm'),
            'all_items' => __('All Staff', 'healthnetwm'),
            'view_item' => __('View Staff', 'healthnetwm'),
            'add_new_item' => __('Add New Staff', 'healthnetwm'),
            'add_new' => __('Add New', 'healthnetwm'),
            'edit_item' => __('Edit Staff', 'healthnetwm'),
            'update_item' => __('Update Staff', 'healthnetwm'),
            'search_items' => __('Search Staff', 'healthnetwm'),
            'not_found' => __('Not Found', 'healthnetwm'),
            'not_found_in_trash' => __('Not found in Trash', 'healthnetwm')
        ],
        'supports' => [
            'title',
            'editor',
            'revisions',
            'custom-fields',
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_in_admin_bar' => true,
        'menu_position' => 40,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    ];
    register_post_type('staff', $staff);

    $quote = [
        'label' => __('quote', 'healthnetwm'),
        'description' => __('Quotes for homepage', 'healthnetwm'),
        'labels' => [
            'name' => _x('Quote', 'Post Type General Name', 'healthnetwm'),
            'singular_name' => _x('Quote', 'Post Type Singular Name', 'healthnetwm'),
            'menu_name' => __('Quotes', 'healthnetwm'),
            'parent_item_colon' => __('Parent Quote', 'healthnetwm'),
            'all_items' => __('All Quotes', 'healthnetwm'),
            'view_item' => __('View Quote', 'healthnetwm'),
            'add_new_item' => __('Add New Quote', 'healthnetwm'),
            'add_new' => __('Add New', 'healthnetwm'),
            'edit_item' => __('Edit Quote', 'healthnetwm'),
            'update_item' => __('Update Quote', 'healthnetwm'),
            'search_items' => __('Search Quote', 'healthnetwm'),
            'not_found' => __('Not Found', 'healthnetwm'),
            'not_found_in_trash' => __('Not found in Trash', 'healthnetwm')
        ],
        'supports' => [
            'title',
            'editor',
            'revisions',
            'custom-fields',
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => false,
        'show_in_admin_bar' => true,
        'menu_position' => 50,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    ];
    register_post_type('quote', $quote);

    $fact = [
        'label' => __('fact', 'healthnetwm'),
        'description' => __('Facts for homepage', 'healthnetwm'),
        'labels' => [
            'name' => _x('Fact', 'Post Type General Name', 'healthnetwm'),
            'singular_name' => _x('Fact', 'Post Type Singular Name', 'healthnetwm'),
            'menu_name' => __('Facts', 'healthnetwm'),
            'parent_item_colon' => __('Parent Fact', 'healthnetwm'),
            'all_items' => __('All Facts', 'healthnetwm'),
            'view_item' => __('View Facts', 'healthnetwm'),
            'add_new_item' => __('Add New Fact', 'healthnetwm'),
            'add_new' => __('Add New', 'healthnetwm'),
            'edit_item' => __('Edit Fact', 'healthnetwm'),
            'update_item' => __('Update Fact', 'healthnetwm'),
            'search_items' => __('Search Fact', 'healthnetwm'),
            'not_found' => __('Not Found', 'healthnetwm'),
            'not_found_in_trash' => __('Not found in Trash', 'healthnetwm')
        ],
        'supports' => [
            'title',
            'editor',
            'revisions',
            'custom-fields',
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => false,
        'show_in_admin_bar' => true,
        'menu_position' => 50,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => true,
        'publicly_queryable' => true,
        'capability_type' => 'page',
    ];
    register_post_type('fact', $fact);
}

/* Hook into the 'init' action so that the function
* Containing our post type registration is not
* unnecessarily executed.
*/

add_action('init', 'custom_post_type', 0);

// Register Navigation Menus
register_nav_menus([
    'main_nav' => 'Main Site Navigation',
    'top_nav' => 'Utility Menu',
]);

require(get_template_directory() . '/includes/theme-options.php');

/**
 * Register our sidebars and widgetized areas.
 */
function healthnetwm_widgets_init() {
    register_sidebar([
        'name' => 'Home Widgets',
        'id' => 'home_widgets',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>',
    ]);
    register_sidebar([
        'name' => 'Left Sidebar Widgets',
        'id' => 'left_widgets',
        'before_widget' => '<div>',
        'after_widget' => '</div>',
        'before_title' => '<h2 class="rounded">',
        'after_title' => '</h2>',
    ]);
}
add_action('widgets_init', 'healthnetwm_widgets_init');

function getRelativeUrl($url)
{
    return (home_url() !== wp_guess_url()) ? str_replace(wp_guess_url(), home_url(), $url) : $url;
}

/**
 * Display navigation to next/previous set of posts when applicable.
 *
 * @since HealthNetWM 1.0
 *
 * @global WP_Query   $wp_query   WordPress Query object.
 * @global WP_Rewrite $wp_rewrite WordPress Rewrite object.
 */
function healthnetwm_paging_nav() {
    global $wp_query, $wp_rewrite;

    // Don't print empty markup if there's only one page.
    if ( $wp_query->max_num_pages < 2 ) {
        return;
    }

    $paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
    $pagenum_link = html_entity_decode( get_pagenum_link() );
    $query_args   = array();
    $url_parts    = explode( '?', $pagenum_link );

    if ( isset( $url_parts[1] ) ) {
        wp_parse_str( $url_parts[1], $query_args );
    }

    $pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
    $pagenum_link = trailingslashit( $pagenum_link ) . '%_%';

    $format  = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
    $format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';

    // Set up paginated links.
    $links = paginate_links( array(
        'base'     => $pagenum_link,
        'format'   => $format,
        'total'    => $wp_query->max_num_pages,
        'current'  => $paged,
        'mid_size' => 1,
        'add_args' => array_map( 'urlencode', $query_args ),
        'prev_text' => __( '&larr; Previous', 'healthnetwm' ),
        'next_text' => __( 'Next &rarr;', 'healthnetwm' ),
    ) );

    if ( $links ) :

        ?>
        <nav class="navigation paging-navigation" role="navigation">
            <h1 class="screen-reader-text"><?php _e( 'Posts navigation', 'healthnetwm' ); ?></h1>
            <div class="pagination loop-pagination">
                <?php echo $links; ?>
            </div><!-- .pagination -->
        </nav><!-- .navigation -->
    <?php
    endif;
}

require __DIR__ . '/slider/functions.php';
require __DIR__ . '/facts/functions.php';
require __DIR__ . '/quotes/functions.php';
