<?php

$facts = new WP_Query(['post_type' => 'fact', 'orderby' => 'rand', 'posts_per_page' => 3]);

/**
 * Setup the default fields
 */
$factTitle = '';

$columns = ['left', 'middle', 'right'];
$counter = 0;
?>
<?php if ($facts->have_posts()) : ?>
    <section id="facts">
        <?php while($facts->have_posts()) : ?>
            <?php $facts->the_post() ?>
            <?php extract(getFactFields($facts), EXTR_OVERWRITE) ?>
            <div class="<?= $columns[$counter] ?>">
                <h2><?= $factTitle ?></h2>
                <p><?php the_content() ?></p>
            </div>
            <?php $counter++ ?>
            <?php wp_reset_postdata() ?>
        <?php endwhile ?>
    </section>
<?php endif ?>
