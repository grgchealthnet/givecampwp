<?php

/**
 * Include slider classes
 */
require_once __DIR__ . '/classes/CalculateRgb.php';

/**
 * Get all of the slider fields
 *
 * @param WP_Query $query
 * @return array
 */
function getSliderFields(WP_Query $query)
{
    $fields = [];
    $fieldResults = get_fields($query->get_queried_object()->ID);
    foreach ($fieldResults as $field => $value) {
        $field = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $field))));
        $fields[$field] = $value;
    }
    return $fields;
}

/**
 * @param string $hexColor
 * @param float $opacity
 * @return string
 */
function getCalculatedPalette($hexColor, $opacity = .75)
{
    $palette = (new CalculateRgb($hexColor))->calculateRgb();

    $red = $palette->getRed();
    $green = $palette->getGreen();
    $blue = $palette->getBlue();

    return "rgba($red, $green, $blue, $opacity)";
}

/**
 * Calculate the RGBA background color based on hexidecimal color
 *
 * @param string $hexColor
 * @param float $opacity
 * @return string
 */
function getRgbaBackground($hexColor, $opacity = .75)
{
    $palette = (new CalculateRgb($hexColor))->calculateRgb();

    $red = $palette->getRed();
    $green = $palette->getGreen();
    $blue = $palette->getBlue();

    return "background: rgba($red, $green, $blue, $opacity);";
}

/**
 * @param string $hexColor
 * @param int $level
 * @param int $type
 * @return string
 */
function getButtonGradient($hexColor, $level, $type)
{
    $calculate = (new CalculateRgb($hexColor));

    $beginColorPalette = $calculate->calculateRgb();

    if ($type === CalculateRgb::TYPE_DARKEN) {
        $endColorPalette = $calculate->darkenColor($beginColorPalette, $level);
    }
    if ($type === CalculateRgb::TYPE_LIGHTEN) {
        $endColorPalette = $calculate->lightenColor($beginColorPalette, $level);
    }

    $beginRed = $beginColorPalette->getRed();
    $beginGreen = $beginColorPalette->getGreen();
    $beginBlue = $beginColorPalette->getBlue();
    $beginColor = "rgba($beginRed, $beginGreen, $beginBlue, 1)";

    $endRed = $endColorPalette->getRed();
    $endGreen = $endColorPalette->getGreen();
    $endBlue = $endColorPalette->getBlue();
    $endColor =  "rgba($endRed, $endGreen, $endBlue, 1)";

    $style = '';
    $style .= 'background: -webkit-linear-gradient('. $beginColor .', '. $endColor .') !important; ';
    $style .= 'background: -o-linear-gradient('. $beginColor .', '. $endColor .') !important; ';
    $style .= 'background: -moz-linear-gradient('. $beginColor .', '. $endColor .') !important; ';
    $style .= 'background: linear-gradient('. $beginColor .', '. $endColor .') !important; ';

    return $style;
}
