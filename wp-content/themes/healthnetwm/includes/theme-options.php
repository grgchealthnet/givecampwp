<?php

// Exit if accessed directly
if ( ! defined ( 'ABSPATH' ) )
{
    header ( "HTTP/1.0 403 Forbidden" );
    require_once ( $_SERVER [ 'DOCUMENT_ROOT' ] . '/wp-blog-header.php' );
    die ( 'Direct file access prohibited.  Please go back to the <a href="' . site_url () . '">homepage</a> and try again.' );
}

/**
 * Theme Options
 */

add_action ( 'admin_init', 'healthnetwm_theme_options_init' );
add_action ( 'admin_menu', 'healthnetwm_theme_options_add_page' );


/**
 * A safe way of adding JavaScripts to a WordPress generated page.
 */
function healthnetwm_admin_enqueue_scripts ( $hook_suffix )
{
    $templateIncludes = get_template_directory_uri () . '/includes';

    wp_enqueue_style ( 'healthnetwm-theme-options', $templateIncludes . '/theme-options.css', false, '1.0' );
    wp_enqueue_script ( 'healthnetwm-theme-options', $templateIncludes . '/theme-options.js', array ( 'jquery' ), '1.0' );
    wp_enqueue_script ( 'jquery' );
    wp_enqueue_script ( 'thickbox' );
    wp_enqueue_style ( 'thickbox' );
    //wp_enqueue_script ( 'media-upload' );
    wp_enqueue_media();
}
add_action ( 'admin_print_styles-appearance_page_theme_options', 'healthnetwm_admin_enqueue_scripts' );

/**
 * Init plugin options to white list our options
 */
function healthnetwm_theme_options_init ()
{
    register_setting ( 'healthnetwm_options', 'healthnetwm_theme_options', 'healthnetwm_theme_options_validate' );
}

/**
 * Load up the menu page
 */
function healthnetwm_theme_options_add_page() {
    add_theme_page(__('Theme Options', 'healthnetwm'), __('Theme Options', 'healthnetwm'), 'edit_theme_options', 'theme_options', 'healthnetwm_theme_options_do_page');
}

/**
 * Create the options page
 */
function healthnetwm_theme_options_do_page() {

    if (!isset($_REQUEST['settings-updated']))
        $_REQUEST['settings-updated'] = false;
    ?>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" />
    <script src="http://code.jquery.com/ui/1.9.1/jquery-ui.js"></script>
    <style>
    #sortable { list-style-type: none; margin: 0; padding: 0; }
    #sortable li {/* margin: 0 5px 5px 5px;*/ padding: 5px; /*font-size: 1.2em;*/ height: 1.5em; cursor:move; }
    html>body #sortable li { height: 1.5em; line-height: 1.2em; }
    .ui-state-highlight { height: 1.5em; line-height: 1.2em; }
    </style>
    <div class="wrap">
        <?php
        /**
         * < 3.4 Backward Compatibility
         */
        ?>
        <?php $theme_name = function_exists('wp_get_theme') ? wp_get_theme() : get_current_theme(); ?>
        <?php //screen_icon();
        echo "<h2>HealthNetWM ". __('Theme Options', 'healthnetwm') . "</h2>";
        $current_user = wp_get_current_user();
        $user = $current_user->ID;
        ?>



        <?php if (false !== $_REQUEST['settings-updated']) : ?>
        <div class="updated fade"><p><strong><?php _e('Options Saved', 'healthnetwm'); ?></strong></p></div>
        <?php endif; ?>


        <form method="post" action="options.php">
            <?php settings_fields('healthnetwm_options'); ?>
            <?php $options = get_option('healthnetwm_theme_options'); ?>

            <p class="submit">
                <input type="submit" class="button-primary" value="<?php _e('Save Options', 'healthnetwm'); ?>" />
            </p>

            <h3 class="rwd-toggle"><a href="#"><?php _e('Home Page', 'healthnetwm'); ?></a></h3>
            <div class="rwd-container" id="home-options">
                <div class="rwd-block">
                    <?php
                    /**
                    * Home Page Specific Options
                    */
                    ?>
                    <div class="grid col-300"><?php _e('Top Left Title', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <input id="healthnetwm_theme_options[top_left_title]" class="regular-text" type="text" name="healthnetwm_theme_options[top_left_title]" value="<?php if (!empty($options['top_left_title'])) echo esc_attr($options['top_left_title']); ?>" />
                        <label class="description" for="healthnetwm_theme_options[top_left_title]"><?php _e('Enter the title for the Top Left Callout', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Top Left Content Area', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <textarea id="healthnetwm_theme_options[top_left_content_area]" class="large-text" cols="50" rows="10" name="healthnetwm_theme_options[top_left_content_area]"><?php if (!empty($options['top_left_content_area'])) echo esc_html($options['top_left_content_area']); ?></textarea>
                        <label class="description" for="healthnetwm_theme_options[top_left_content_area]"><?php _e('Enter your content', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Top Middle Title', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <input id="healthnetwm_theme_options[top_middle_title]" class="regular-text" type="text" name="healthnetwm_theme_options[top_middle_title]" value="<?php if (!empty($options['top_middle_title'])) echo esc_attr($options['top_middle_title']); ?>" />
                        <label class="description" for="healthnetwm_theme_options[top_middle_title]"><?php _e('Enter the title for the Top Middle Callout', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Top Middle Content Area', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <textarea id="healthnetwm_theme_options[top_middle_content_area]" class="large-text" cols="50" rows="10" name="healthnetwm_theme_options[top_middle_content_area]"><?php if (!empty($options['top_middle_content_area'])) echo esc_html($options['top_middle_content_area']); ?></textarea>
                        <label class="description" for="healthnetwm_theme_options[top_middle_content_area]"><?php _e('Enter your content', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Top Right Title', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <input id="healthnetwm_theme_options[top_right_title]" class="regular-text" type="text" name="healthnetwm_theme_options[top_right_title]" value="<?php if (!empty($options['top_right_title'])) echo esc_attr($options['top_right_title']); ?>" />
                        <label class="description" for="healthnetwm_theme_options[top_right_title]"><?php _e('Enter the title for the Top Right Callout', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Top Right Content Area', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <textarea id="healthnetwm_theme_options[top_right_content_area]" class="large-text" cols="50" rows="10" name="healthnetwm_theme_options[top_right_content_area]"><?php if (!empty($options['top_right_content_area'])) echo esc_html($options['top_right_content_area']); ?></textarea>
                        <label class="description" for="healthnetwm_theme_options[top_right_content_area]"><?php _e('Enter your content', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <!-- Bottom -->

                    <div class="grid col-300"><?php _e('Bottom Left Image', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <?php if (!empty($options['bottom_left_image'])) echo '<img src="' . esc_attr($options['bottom_left_image']) . '" style="width:50px; height:50px;"/>'; ?>
                        <input id="bottom_left_image" class="regular-text" name="healthnetwm_theme_options[bottom_left_image]" type="text" value="<?php if (!empty($options['bottom_left_image'])) echo esc_attr($options['bottom_left_image']); ?>" />
                        <input id="bottom_left_image_button" class="button-secondary" name="bottom_left_image_button" type="text" value="Upload" />

                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Bottom Left Title', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <input id="healthnetwm_theme_options[bottom_left_title]" class="regular-text" type="text" name="healthnetwm_theme_options[bottom_left_title]" value="<?php if (!empty($options['bottom_left_title'])) echo esc_attr($options['bottom_left_title']); ?>" />
                        <label class="description" for="healthnetwm_theme_options[bottom_left_title]"><?php _e('Enter the title for the Bottom Left Callout', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Bottom Left Content Area', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <textarea id="healthnetwm_theme_options[bottom_left_content_area]" class="large-text" cols="50" rows="10" name="healthnetwm_theme_options[bottom_left_content_area]"><?php if (!empty($options['bottom_left_content_area'])) echo esc_html($options['bottom_left_content_area']); ?></textarea>
                        <label class="description" for="healthnetwm_theme_options[bottom_left_content_area]"><?php _e('Enter your content', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Bottom Left URL', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <input id="healthnetwm_theme_options[bottom_left_url]" class="regular-text" type="text" name="healthnetwm_theme_options[bottom_left_url]" value="<?php if (!empty($options['bottom_left_url'])) echo esc_attr($options['bottom_left_url']); ?>" />
                        <label class="description" for="healthnetwm_theme_options[bottom_left_url]"><?php _e('Enter the Bottom Left URL', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Bottom Middle Image', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <?php if (!empty($options['bottom_middle_image'])) echo '<img src="' . esc_attr($options['bottom_middle_image']) . '" style="width:50px; height:50px;"/>'; ?>
                        <input id="bottom_middle_image" class="regular-text" name="healthnetwm_theme_options[bottom_middle_image]" type="text" value="<?php if (!empty($options['bottom_middle_image'])) echo esc_attr($options['bottom_middle_image']); ?>" />
                        <input id="bottom_middle_image_button" class="button-secondary" name="bottom_middle_image_button" type="text" value="Upload" />

                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Bottom Middle Title', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <input id="healthnetwm_theme_options[bottom_middle_title]" class="regular-text" type="text" name="healthnetwm_theme_options[bottom_middle_title]" value="<?php if (!empty($options['bottom_middle_title'])) echo esc_attr($options['bottom_middle_title']); ?>" />
                        <label class="description" for="healthnetwm_theme_options[bottom_middle_title]"><?php _e('Enter the title for the Bottom Middle Callout', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Bottom Middle Content Area', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <textarea id="healthnetwm_theme_options[bottom_middle_content_area]" class="large-text" cols="50" rows="10" name="healthnetwm_theme_options[bottom_middle_content_area]"><?php if (!empty($options['bottom_middle_content_area'])) echo esc_html($options['bottom_middle_content_area']); ?></textarea>
                        <label class="description" for="healthnetwm_theme_options[bottom_middle_content_area]"><?php _e('Enter your content', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Bottom Middle URL', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <input id="healthnetwm_theme_options[bottom_middle_url]" class="regular-text" type="text" name="healthnetwm_theme_options[bottom_middle_url]" value="<?php if (!empty($options['bottom_middle_url'])) echo esc_attr($options['bottom_middle_url']); ?>" />
                        <label class="description" for="healthnetwm_theme_options[bottom_middle_url]"><?php _e('Enter the Bottom Middle URL', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Bottom Right Image', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <?php if (!empty($options['bottom_right_image'])) echo '<img src="' . esc_attr($options['bottom_right_image']) . '" style="width:50px; height:50px;"/>'; ?>
                        <input id="bottom_right_image" class="regular-text" name="healthnetwm_theme_options[bottom_right_image]" type="text" value="<?php if (!empty($options['bottom_right_image'])) echo esc_attr($options['bottom_right_image']); ?>" />
                        <input id="bottom_right_image_button" class="button-secondary" name="bottom_right_image_button" type="text" value="Upload" />

                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Bottom Right Title', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <input id="healthnetwm_theme_options[bottom_right_title]" class="regular-text" type="text" name="healthnetwm_theme_options[bottom_right_title]" value="<?php if (!empty($options['bottom_right_title'])) echo esc_attr($options['bottom_right_title']); ?>" />
                        <label class="description" for="healthnetwm_theme_options[bottom_right_title]"><?php _e('Enter the title for the Bottom Right Callout', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Bottom Right Content Area', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <textarea id="healthnetwm_theme_options[bottom_right_content_area]" class="large-text" cols="50" rows="10" name="healthnetwm_theme_options[bottom_right_content_area]"><?php if (!empty($options['bottom_right_content_area'])) echo esc_html($options['bottom_right_content_area']); ?></textarea>
                        <label class="description" for="healthnetwm_theme_options[bottom_right_content_area]"><?php _e('Enter your content', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Bottom Right URL', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <input id="healthnetwm_theme_options[bottom_right_url]" class="regular-text" type="text" name="healthnetwm_theme_options[bottom_right_url]" value="<?php if (!empty($options['bottom_right_url'])) echo esc_attr($options['bottom_right_url']); ?>" />
                        <label class="description" for="healthnetwm_theme_options[bottom_right_url]"><?php _e('Enter the Bottom Right URL', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <p class="submit">
                        <input type="submit" class="button-primary" value="<?php _e('Save Options', 'healthnetwm'); ?>" />
                    </p>

                </div><!-- end of .rwd-block -->
            </div><!-- end of .rwd-container -->

            <h3 class="rwd-toggle"><a href="#"><?php _e('Company Information', 'healthnetwm'); ?></a></h3>
            <div class="rwd-container">
                <div class="rwd-block">
                    <?php
                    /**
                    * Default Contact Information
                    */
                    ?>
                    <div class="grid col-300"><?php _e('Address', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <input id="healthnetwm_theme_options[address]" class="regular-text" type="text" name="healthnetwm_theme_options[address]" value="<?php if (!empty($options['address'])) echo esc_attr($options['address']); ?>" />
                        <label class="description" for="healthnetwm_theme_options[address]"><?php _e('Enter your address', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Address 2', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <input id="healthnetwm_theme_options[address2]" class="regular-text" type="text" name="healthnetwm_theme_options[address2]" value="<?php if (!empty($options['address2'])) echo esc_attr($options['address2']); ?>" />
                        <label class="description" for="healthnetwm_theme_options[address2]"><?php _e('Address 2', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('City', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <input id="healthnetwm_theme_options[city]" class="regular-text" type="text" name="healthnetwm_theme_options[city]" value="<?php if (!empty($options['city'])) echo esc_attr($options['city']); ?>" />
                        <label class="description" for="healthnetwm_theme_options[city]"><?php _e('Enter your city', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('State', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <input id="healthnetwm_theme_options[state]" class="regular-text" type="text" name="healthnetwm_theme_options[state]" value="<?php if (!empty($options['state'])) echo esc_attr($options['state']); ?>" />
                        <label class="description" for="healthnetwm_theme_options[state]"><?php _e('Enter your state', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Zip Code', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <input id="healthnetwm_theme_options[zip]" class="regular-text" type="text" name="healthnetwm_theme_options[zip]" value="<?php if (!empty($options['zip'])) echo esc_attr($options['zip']); ?>" />
                        <label class="description" for="healthnetwm_theme_options[zip]"><?php _e('Enter your zip', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Phone #', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <input id="healthnetwm_theme_options[phone]" class="regular-text" type="text" name="healthnetwm_theme_options[phone]" value="<?php if (!empty($options['phone'])) echo esc_attr($options['phone']); ?>" />
                        <label class="description" for="healthnetwm_theme_options[phone]"><?php _e('Enter your phone', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Fax #', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <input id="healthnetwm_theme_options[fax]" class="regular-text" type="text" name="healthnetwm_theme_options[fax]" value="<?php if (!empty($options['fax'])) echo esc_attr($options['fax']); ?>" />
                        <label class="description" for="healthnetwm_theme_options[fax]"><?php _e('Enter your fax', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <div class="grid col-300"><?php _e('Mission Statement', 'healthnetwm'); ?></div><!-- end of .grid col-300 -->
                    <div class="grid col-620 fit">
                        <textarea id="healthnetwm_theme_options[mission]" class="large-text" cols="50" rows="10" name="healthnetwm_theme_options[mission]"><?php if (!empty($options['mission'])) echo esc_html($options['mission']); ?></textarea>
                        <label class="description" for="healthnetwm_theme_options[mission]"><?php _e('Enter your mission statement', 'healthnetwm'); ?></label>
                    </div><!-- end of .grid col-620 -->

                    <p class="submit">
                        <input type="submit" class="button-primary" value="<?php _e('Save Options', 'healthnetwm'); ?>" />
                    </p>

                </div><!-- end of .rwd-block -->
            </div><!-- end of .rwd-container -->

            <h3 class="rwd-toggle"><a href="#"><?php _e('Social', 'healthnetwm'); ?></a></h3>
            <div class="rwd-container">
                <div class="rwd-block">
                    <p>Drag and drop to change the order that each icon will display on the front end.</p>
                    <p style="text-align:right;">Active?</p>
                 <ul id="sortable">
                <?php
                /**
                 * Social Media
                 */
                $socialArray = array(
                    'facebook_uid'      => $options['social']['facebook_uid'],
                    'twitter_uid'       => $options['social']['twitter_uid'],
                    'linkedin_uid'      => $options['social']['linkedin_uid'],
                    'youtube_uid'       => $options['social']['youtube_uid'],
                    'stumble_uid'       => $options['social']['stumble_uid'],
                    'rss_uid'           => $options['social']['rss_uid'],
                    'google_plus_uid'   => $options['social']['google_plus_uid'],
                    'instagram_uid'     => $options['social']['instagram_uid'],
                    'pinterest_uid'     => $options['social']['pinterest_uid'],
                    'yelp_uid'          => $options['social']['yelp_uid'],
                    'vimeo_uid'         => $options['social']['vimeo_uid'],
                    'foursquare_uid'    => $options['social']['foursquare_uid']
                    );
                if ($options['social'])
                {
                    $newSocialArray = array_merge($options['social'], $socialArray);
                }
                else
                {
                    $newSocialArray = $socialArray;
                }
                if (is_array($newSocialArray))
                {
                    $icon = '';
                    $active = '';
                    $title = '';
                    $image = '';
                    foreach ($newSocialArray as $key => $data)
                    {
                        $keys[] = $key;
                        $parts = explode('_',$key);
                        $name = $parts[0];
                        $title = ucfirst($name);
                        $image = get_stylesheet_directory_uri().'/icons/'.$name.'-icon.png';
                        if (isset($data['icon']) && $data['icon'] != '')
                        {
                            $icon = $data['icon'];
                        }
                        else
                        {
                            $icon = $image;
                        }
                        if (isset($data['active']) && $data['active'] == 1)
                        {
                            $checked = 'checked="checked"';
                            $activeTitle = "Deactivate?";
                        }
                        else
                        {
                            $checked = '';
                            $activeTitle = 'Activate?';
                        }
                        echo <<<HTML
                        <li class="ui-state-default" id="ordinal_1">
                        <div class="grid col-60 fit" style="position:relative;">
                            <div id="{$name}_image_holder" style="display:none;"></div>
                            <img src="$icon" width="15" height="15" alt="$name" title="Click to update this icon" id="{$name}_icon_holder" class="icon_holder" />
                            <input type="text" name="healthnetwm_theme_options[social][$key][icon]" value="{$data['icon']}" id="{$name}_icon" rel="$name" class="icon_upload" style="position:absolute; z-index:10; left:20px; display:none;" />
                            <input id="{$name}" class="upload_button" type="button" value="Upload" style="position:absolute; z-index:10; top:-4px; left:155px; display:none;" />
                        </div>
                        <div class="grid col-300">$title</div><!-- end of .grid col-300 -->
                            <div class="grid col-540 fit">
                                <input id="healthnetwm_theme_options[social][$key][url]" class="regular-text" type="text" name="healthnetwm_theme_options[social][$key][url]" value="{$data['url']}" />
                                <label class="description" for="healthnetwm_theme_options[social][$key][url]">Enter your $title url</label>

                            </div>
                        <div class="squaredOne">
                            <input type="checkbox" value="1" $checked style="display:none;" id="healthnetwm_theme_options[social][$key][active]" name="healthnetwm_theme_options[social][$key][active]" />
                            <label for="healthnetwm_theme_options[social][$key][active]"></label>
                        </div>
                        </li>

HTML;
                    }
                }
                else
                {
                }
                ?>
                    </ul>
                <p class="submit">
                    <input type="submit" class="button-primary" value="<?php _e('Save Options', 'healthnetwm'); ?>" />
                </p>

                </div><!-- end of .rwd-block -->
            </div><!-- end of .rwd-container -->

            <h3 class="rwd-toggle"><a href="#"><?php _e('Developer', 'healthnetwm'); ?></a></h3>
            <div class="rwd-container">
                <div class="rwd-block">

                    <h3>Complete options array:</h3>
                    <?php echo '<pre>'; print_r($options); echo '</pre>'; ?>
                </div>
            </div>
            <!-- end of .grid col-940 -->
        </form>
    </div>
    <?php
}

/**
 * Sanitize and validate input. Accepts an array, return a sanitized array.
 */
function healthnetwm_theme_options_validate($input) {

    // checkbox value is either 0 or 1
    if (isset($input['twitter_uid'])) {$input['twitter_uid'] = esc_url_raw($input['twitter_uid']); }
    if (isset($input['facebook_uid'])) {$input['facebook_uid'] = esc_url_raw($input['facebook_uid']); }
    if (isset($input['linkedin_uid'])) {$input['linkedin_uid'] = esc_url_raw($input['linkedin_uid']); }
    if (isset($input['youtube_uid'])) {$input['youtube_uid'] = esc_url_raw($input['youtube_uid']); }
    if (isset($input['stumble_uid'])) {$input['stumble_uid'] = esc_url_raw($input['stumble_uid']); }
    if (isset($input['rss_uid'])) {$input['rss_uid'] = esc_url_raw($input['rss_uid']); }
    if (isset($input['google_plus_uid'])) {$input['google_plus_uid'] = esc_url_raw($input['google_plus_uid']); }
    if (isset($input['instagram_uid'])) {$input['instagram_uid'] = esc_url_raw($input['instagram_uid']); }
    if (isset($input['pinterest_uid'])) {$input['pinterest_uid'] = esc_url_raw($input['pinterest_uid']); }
    if (isset($input['yelp_uid'])) {$input['yelp_uid'] = esc_url_raw($input['yelp_uid']); }
    if (isset($input['vimeo_uid'])) {$input['vimeo_uid'] = esc_url_raw($input['vimeo_uid']); }
    if (isset($input['foursquare_uid'])) {$input['foursquare_uid'] = esc_url_raw($input['foursquare_uid']); }

    return $input;
}
