<form action="/" method="get">
    <fieldset class="combo">
        <input type="text" name="s" id="search" value="<?php the_search_query(); ?>" placeholder="Search" size="30"/>
        <button type="submit" class="fa fa-search"></button>
    </fieldset>
</form>
