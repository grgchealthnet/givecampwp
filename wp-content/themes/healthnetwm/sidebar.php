<?php
/**
 * The Sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage HealthNetWM
 * @since Health Net WM 1.0
 */
?>
<div id="secondary">
    <?php if ( has_nav_menu( 'secondary' ) ) : ?>
        <nav role="navigation" class="navigation site-navigation secondary-navigation">
            <?php wp_nav_menu( array( 'theme_location' => 'secondary' ) ); ?>
        </nav>
    <?php endif; ?>

    <?php if ( is_active_sidebar( 'left_widgets' ) ) : ?>
    <div id="primary-widgets" class="widget-area" role="complementary">
        <?php dynamic_sidebar( 'left_widgets' ); ?>
    </div>
    <?php endif; ?>
</div>