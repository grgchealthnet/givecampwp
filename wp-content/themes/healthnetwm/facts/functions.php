<?php

/**
 * Get all of the slider fields
 *
 * @param WP_Query $query
 * @return array
 */
function getFactFields(WP_Query $query)
{
    $fields = [];
    $fieldResults = get_fields($query->get_queried_object()->ID);
    foreach ($fieldResults as $field => $value) {
        $field = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $field))));
        $fields[$field] = $value;
    }
    return $fields;
}