<?php
add_action('init', 'ninja_forms_register_display_hidden_fields');
function ninja_forms_register_display_hidden_fields(){
	add_action('ninja_forms_display_after_open_form_tag', 'ninja_forms_display_hidden_fields');
}

function ninja_forms_display_hidden_fields($form_id){
	global $ninja_forms_processing;
	?>
	<input type="hidden" name="_ninja_forms_display_submit" value="1">
	<input type="hidden" name="_form_id"  id="_form_id" value="<?php echo $form_id;?>">
	<?php
	$form_row = ninja_forms_get_form_by_id( $form_id );
	$paypalAccount = $form_row['data']['paypal_account'];
	$paypalReturn = $form_row['data']['paypal_return'];
	$paypalSuccess = $form_row['data']['paypal_success'];

	if ( !empty($paypalAccount) )
	{
	?>
		<input type="hidden" value="_xclick" name="cmd" />
		<input type="hidden" value="<?php echo $paypalAccount; ?>" name="business" />
		<input type="hidden" value="<?php echo $paypalSuccess; ?>" name="return" />
		<input type="hidden" value="<?php echo $paypalReturn; ?>" name="cancel_return" />
		<input type="hidden" name="handling_cart" value="0.00">
		<input type="hidden" name="tax_cart" value="0.00">
		<input type="hidden" name="no_shipping" value="1">
		<input type="hidden" name="upload" value="1" >';
	<?php
	}
	?>

	<?php
	if( is_object( $ninja_forms_processing) AND $ninja_forms_processing->get_all_errors()){
		?>
		<input type="hidden" id="ninja_forms_processing_error" value="1">
		<?php
	}
}