   ___ _               ___                        ____   ___  _ _  _
  / _ (_)_   _____    / __\__ _ _ __ ___  _ __   |___ \ / _ \/ | || |
 / /_\/ \ \ / / _ \  / /  / _` | '_ ` _ \| '_ \    __) | | | | | || |_
/ /_\\| |\ V /  __/ / /__| (_| | | | | | | |_) |  / __/| |_| | |__   _|
\____/|_| \_/ \___| \____/\__,_|_| |_| |_| .__/  |_____|\___/|_|  |_|
                                         |_|

Site created at GR Give Camp, November 2014
============================

THE TEAM:
-----------

Aaron Dake
Andrea Napierkowski
Beca Velazquez-Publes
Chris Lewis
Chris Snyder - snyder616.com
David Tarnow (C2 Group)
Jaime Wise
Sarah Wilson
Josh Morningstar - @the_deuceII
Pete Buzzell - @pbuzzell
John Miller (C2 Group)
