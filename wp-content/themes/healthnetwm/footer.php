<div class="clearfix"> </div>
</div> <?php // #main-content-container opened in header.php ?>
</div> <?php // #page-wrapper opened in header.php ?>

<div id="bottom-footer-container">
<footer id="bottom-footer">
<?php
$options = get_option('healthnetwm_theme_options');
if ($options && !empty($options)):
    ?>
<section id="social">
    <?php foreach($options['social'] as $social) :?>
        <a href="<?= $social['url'] ?>" target="_blank"><img src="<?= $social['icon'] ?>" alt="<?= $social['title'] ?>"></a>
    <?php endforeach ?>
</section>
<section id="bottom-contact-info">
    <p><?php echo $options['address']; ?></p>
    <p><?php echo $options['city'] . ', ' . $options['state'] . ' ' . $options['zip']; ?></p>
    <p class="phone"><?php echo $options['phone']; ?></p>
</section>
<?php endif; ?>
<section id="bottom-copyright-notice">
© <?php echo date('Y');?> Health Net of West Michigan</section>
</footer>
</div>

<!-- Analytics -->
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-57084136-1', 'auto');
    ga('send', 'pageview');
</script>
<!-- End Analytics -->

<?php wp_footer(); ?>
</body>
</html>