<?php

require __DIR__ . '/RgbPalette.php';

/**
 * Class CalculateRgb
 */
class CalculateRgb
{
    const TYPE_DARKEN = 1;
    const TYPE_LIGHTEN = 2;

    /**
     * @var RgbPalette
     */
    protected $rgbPalette;

    /**
     * @var string
     */
    protected $hexColor;

    /**
     * @var array
     */
    protected $rgbColor;

    /**
     * @param string $hexColor
     */
    public function __construct($hexColor)
    {
        $this->hexColor = str_replace("#", "", $hexColor);
        $this->rgbPalette = new RgbPalette();
    }

    /**
     * @return null|RgbPalette
     */
    public function calculateRgb()
    {
        if ($this->hexColor) {
            if (strlen($this->hexColor) === 3) {
                return $this->rgbPalette
                    ->setRed(hexdec(str_repeat(substr($this->hexColor, 0, 1), 2)))
                    ->setGreen(hexdec(str_repeat(substr($this->hexColor, 1, 1), 2)))
                    ->setBlue(hexdec(str_repeat(substr($this->hexColor, 2, 1), 2)));
            } else {
                return $this->rgbPalette
                    ->setRed(hexdec(substr($this->hexColor, 0, 2)))
                    ->setGreen(hexdec(substr($this->hexColor, 2, 2)))
                    ->setBlue(hexdec(substr($this->hexColor, 4, 2)));
            }
            return $this->rgbPalette;
        }
        return null;
    }

    /**
     * @param RgbPalette $color
     * @param int $level
     * @return RgbPalette
     */
    public function darkenColor(RgbPalette $color, $level)
    {
        //$red = (($color->getRed() - $level) <= 0) ? 0 : ($color->getRed() - $level);
        //$green = (($color->getGreen() - $level) <= 0) ? 0 : ($color->getGreen() - $level);
        //$blue = (($color->getBlue() - $level) <= 0) ? 0 : ($color->getBlue() - $level);

        return (new RgbPalette())
            ->setRed((($color->getRed() - $level) <= 0) ? 0 : ($color->getRed() - $level))
            ->setGreen((($color->getGreen() - $level) <= 0) ? 0 : ($color->getGreen() - $level))
            ->setBlue((($color->getBlue() - $level) <= 0) ? 0 : ($color->getBlue() - $level));
    }

    /**
     * @param RgbPalette $color
     * @param int $level
     * @return RgbPalette
     */
    public function lightenColor(RgbPalette $color, $level)
    {
        return (new RgbPalette())
            ->setRed((($color->getRed() + $level) > 0) ? abs($color->getRed() + $level) : 0)
            ->setGreen((($color->getGreen() + $level) > 0) ? abs($color->getGreen() + $level) : 0)
            ->setBlue((($color->getBlue() + $level) > 0) ? abs($color->getBlue() + $level) : 0);
    }
}
