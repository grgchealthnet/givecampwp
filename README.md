# Yo, Wassup dawg!?

This is the repo for HealthNet West Michigan's Givecamp 2014 Website

To work locally --
    - Clone the git: ```git clone git@bitbucket.org:grgchealthnet/givecampwp.git```
    - Rename ```wp-config.php.local``` to ```wp-config.php```
    - Update the login credentials via BaseCamp
    - Uncomment ```define('WP_SITEURL', '<localweb>');``` and update to your local domain (e.g. wordpress.local)

Enjoy.
