<?php 
/**
 * Template file.
 *
 * @package WordPress
 * @subpackage HealthNetWM
 * @since Health Net WM 1.0
**/
?>

<?php get_header(); ?>

<div id="main-content"><!-- content wrapper -->
    <div id="primary" class="content-area"><!-- full content area -->
        <div id="content" class="site-content" role="main"><!-- content container -->
        <?php
            // Start the Loop.
            while ( have_posts() ) : the_post();
                // Include the page content template.
                get_template_part( 'content', 'page' );
            endwhile;
        ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>