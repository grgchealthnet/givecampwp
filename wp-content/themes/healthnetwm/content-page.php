<?php
/**
 * The template for displaying the page content
 *
 * @package WordPress
 * @subpackage HealthNetWM
 * @since Health Net WM 1.0
 */
?>

<?php the_content(); ?>