<?php
/**
 * Template Name: Staff Page
 *
 * @package WordPress
 * @subpackage HealthNetWM
 * @since Health Net WM 1.0
 **/
?>

<?php
require(get_template_directory() . '/includes/staff-menu-template.php');
get_header();
?>

    <div id="main-content" class="staff-content"><!-- content wrapper -->
        <h1><?= get_the_title(); ?></h1>
        <div id="primary" class="content-area"><!-- full content area -->
            <div id="content" class="site-content" role="main"><!-- content container -->
                <?php
                // Start the Loop.
                while ( have_posts() ) : the_post();
                    // Include the page content template.
                    get_template_part( 'content', 'page' );
                    wp_nav_menu(['menu' => 'Staff Directory', 'container' => 'div', 'container_class' => 'staff-directory-list', 'echo' => 'true', 'depth' => 0, 'walker' => new Staff_Menu_Walker]);
                endwhile;
                ?>
            </div>
        </div>
    </div>

<?php get_footer(); ?>