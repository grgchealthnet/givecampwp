/*jslint browser: true*/
/*global jQuery, console */
jQuery(document).ready(function ($) {
    'use strict';
    /* --- Accordion ---
     * Example usage:
     * <a href="#" data-accordion-toggle="contentBlockName">Toggle content visibility</a>
     * <div data-accordion-content="contentBlockName" data-accordion-default-show="true">I'm the content! I'm shown by default.</div>
     * <a href="#" data-accordion-toggle="contentBlockName">
     *   I'm a second link that also toggles the same content!
     *   <span class="shownWhenHidden">
     *     I'm an element that is shown only when the content when hidden, because of this CSS:
     *     .accordion-toggle-shown .shownWhenHidden {
     *       display: none;
     *     }
     *   </span>
     *  </a>
     */
    // Hide the accordion contents by default (done here so that content is visible if JS is disabled)
    $("ul[data-accordion-content]").each(function () {
        var contentBlock = $(this);
        var contentName = contentBlock.attr('data-accordion-content');
        var shownByDefault = (contentBlock.attr('data-accordion-default-show') !== undefined);
        var togglers = $('[data-accordion-toggle=' + contentName + ']');
        if (shownByDefault) {
            togglers.addClass('accordion-toggle-shown');
        } else {
            togglers.addClass('accordion-toggle-hidden');
            contentBlock.hide();
        }
    });
    // When a toggle link is clicked, toggle visibility of the associated content blocks
    $('h2[data-accordion-toggle]').on('click', function (event) {
        var toggler = $(this),
            contentName = toggler.attr('data-accordion-toggle'),
            isHidden = toggler.hasClass('accordion-toggle-hidden'),
            togglers = $('[data-accordion-toggle=' + contentName + ']'),
            contentBlocks = $('[data-accordion-content=' + contentName + ']');
        if (isHidden) { // Show
            togglers.removeClass('accordion-toggle-hidden').addClass('accordion-toggle-shown');
            contentBlocks.show('fast');
        } else { // Hide
            togglers.addClass('accordion-toggle-hidden').removeClass('accordion-toggle-shown');
            contentBlocks.hide('fast');
        }
        event.preventDefault();
    });

    $('.hamburger-menu').click(function() {
        if($(this).hasClass('open'))
        {
            $('#main-nav').addClass('expand');
            $('#top-nav').addClass('expand');
            $('#top-search').addClass('expand');
            $(this).removeClass('open');
            $(this).addClass('close');
        }
        else
        {
            $('#main-nav').removeClass('expand');
            $('#top-nav').removeClass('expand');
            $('#top-search').removeClass('expand');
            $(this).removeClass('close');
            $(this).addClass('open');
        }
    });
});